<?php
# Developer setting
header('Access-Control-Allow-Origin: http://localhost');

# Page title
define('TITLE','Beporzó monitoring');
define('DESCRIPTION','Polli-mon Go! adatgyűjtő alkalmazás');

# Weather
# GET API key from openweathermap
define('USE_WEATHER', true);
define('WEATHER_API_KEY','');

# OAuth
define('CLIENT_ID','');
define('CLIENT_SECRET','');

# OpenBioMaps settings
define('PROJECTTABLE','pollimon');
define('URL','openbiomaps.org/projects/pollimon');
define('API_VERSION','2.4');
define('APP_PATH','/projects/pollimon/pwa2');

# WMS cluster
# layer name on OpenBioMaps: layer_data_MY-CLUSTER-LAYER for cname created automatically
# Cluster settings
$wms_cluster = 'my_cluster';
$min_zoom_for_filter = 12;
$distanceInput = 30;
$minDistanceInput = 1;
?>
