<?php
# File version tag
function revx($file) {
    $timestamp = 0;
    if (file_exists("$file")) {
        $fp = fopen("$file", "r");
        $fstat = fstat($fp);
        $timestamp = $fstat['mtime'];
    }
    return $timestamp;
}
/* Create a sylog message for debugging
 * */
function debugx($message, $file = NULL, $line = NULL, $class = "", $method = "") {
    if(is_array($message)) {
        $message = json_encode($message,JSON_PRETTY_PRINT);
    }
    elseif(is_object($message)) {
        $message = json_encode($message,JSON_PRETTY_PRINT);
    }
    $custom_log = "/var/log/openbiomaps.log";

    $pointer = "";
    if($file!=NULL or $line!=NULL)
        $pointer = " $file $line";

    $class .= ($class !== "") ? "::$method" : $method;
    
    if (is_writable($custom_log)) {
        $date = date('M j h:i:s'); 
        error_log("[OBM_".PROJECTTABLE."] $date$pointer $class: $message\n", 3, "/var/log/openbiomaps.log");
    } else {
        openlog("[OBM_".PROJECTTABLE."]", 0, LOG_LOCAL0);
        syslog(LOG_WARNING,$pointer.$message);
        closelog();
    }
}
/* 
 * Include the app/file if exists or the main/file
 * */
function require_check ($file) {

    if (file_exists("app/$file"))
        require "app/$file";
    else if (file_exists("main/$file"))
        require "main/$file";

}
?>
