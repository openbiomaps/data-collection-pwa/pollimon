<?php
# Alpine global variables
# This file has been inculded in index.php
?>

// Some dialog's default state
Alpine.store('showInfo', false);
Alpine.store('showGPSbox', false);

// Geo Location State
Alpine.store('geoLocationOn', false);

// Authentication - if needed
Alpine.store('access_token_state', false);
Alpine.store('tokenStore', {
    token: false,
    async checkToken() {
        var x = getCookie('refresh_token');
        if (x) {
            this.token = await refreshToken(); // Visszatér egy access_tokenn-el
        } else {
            alert('Log in to use this app!');
            console.log('Log in to use this app!');
        }
    }
});

// Welcome dialog
Alpine.data('welcome', () => ({
    welcomeopen: false, // welcome dialog off
    showWelcome() {
        //this.welcomeopen = !localStorage.getItem('hasSeenWelcome');  // Welcome screen disabled
        if (!localStorage.getItem('hasSeenWelcome')) {
            localStorage.setItem('hasSeenWelcome', 'true');
        }
    },
    close() {
        this.welcomeopen = false;
    }
}));

