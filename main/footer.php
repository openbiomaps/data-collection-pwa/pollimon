<?php

?>
<div class="header">

    <div id="Menu" class="Menu" x-show="openMenu">
        <ul>
            <li @click="openMenu = !openMenu, welcomeOpen = true">Welcome</li>
        </ul> 
    </div>
    
    <div class="header-button" @click="openMenu = !openMenu" style='color:lightskyblue'>
        <i :class="{'material-icons': true}" title="Menü" style="vertical-align:bottom">more_vert</i>
    </div> &nbsp;


    <div class="header-button" @click="openForm = !openForm" style='color:lightskyblue'>
        <i :class="{'material-icons': true}" :title="openForm ? 'Térkép' : 'Adatgyűjtő űrlap'" style="vertical-align:bottom" x-text="openForm ? 'map' : 'edit_note'"></i>
    </div> &nbsp;

    <div class="header-button" style='color:lightskyblue'>
        <i :class="{'material-icons': true}" :title="Alpine.store('geoLocationOn') ? 'GPS OK' : 'Nincs GPS'" style="vertical-align:bottom" x-text="Alpine.store('geoLocationOn') ? 'location_on' : 'location_off'"></i>
    </div> &nbsp;
    
    <button id="butInstall" aria-label="Install" title="Telepít" hidden></button>

    <i class='fa-solid fa-eye' id="togglekeep" title="Wake Lock is enabled" style='color:lightskyblue;position:absolute;padding:0.4rem;right:0'></i>
</div>

