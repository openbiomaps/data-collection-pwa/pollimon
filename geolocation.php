
<body>
<div id="loader">
    <h2>Üdvözlünk a <i>Polli-mon Go!</i> alkalmazásban!</h2>
    <p style="font-size:120%;overflow-y:scroll">Ezzel az alkalmazással a beporzó állatokról tudsz adatokat gyűjteni a <a href='https://pollinator-monitoring.hu/pollimon-go'>pollinator-monitoring.hu</a> projekt keretében.
        <br>
        <br>
       Az alkalmazás használatához be kell jelentkezzél a projektbe, amit megtehetesz Google fiók segítségével, vagy <a href='https://openbiomaps.org/projects/pollimon/?registration' target='blank'>kérhetsz meghívót</a> a
       projekt térképes adatbázis oldalán.
        <br>
        <br>
       Az alkalmazás helyadatokat használ a mintavételi hely egyértelmű azonosításához, így az alkalmazás használatához be kell kapcsolnod helyadat kezelést (GPS) és engedélyt kell adnod a böngészöben a helyadatokhoz való hozzáféréshez! Amennyiben nem jelenik meg a helyadat engedélykérés ablak, akkor a böngésző címsorának az elején található többnyire egy alkalmazás engedélyekre vonatkozó menü.<br>
    </p>
    <button id="geoButton" class="pure-button button-success">
        <span id="buttonText">Rendben</span>
        <span id="loadingIcon" class="material-icons" style="display: none;">autorenew</span>
    </button>
</div>


<script>

document.addEventListener("DOMContentLoaded", function() {
    var geoButton = document.getElementById("geoButton");
    var loadingIcon = document.getElementById("loadingIcon");
    var buttonText = document.getElementById("buttonText");

    geoButton.addEventListener("click", function() {
        buttonText.style.display = "none";
        loadingIcon.style.display = "block";

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                console.log("Latitude: " + position.coords.latitude);
                console.log("Longitude: " + position.coords.longitude);
                window.location.href = "https://<?php echo URL . '/' . basename(APP_PATH) ?>/?geolocation=enabled"; // Itt történik az átirányítás az alkalmazás fő oldalára
            }, function(error) {
                console.log("A felhasználó nem engedélyezte a GPS használatát.");
                alert("A felhasználó nem engedélyezte a GPS használatát.");
                buttonText.style.display = "inline";
                loadingIcon.style.display = "none";
            });
        } else {
            console.log("A böngésző nem támogatja a Geolokációt.");
            alert("A böngésző nem támogatja a Geolokációt.");
            buttonText.style.display = "inline";
            loadingIcon.style.display = "none";
        }
    });
});
window.onload = function() {
    var userAgent = navigator.userAgent;
        if (userAgent.includes('Firefox') && userAgent.includes('Mobile')) {
            alert("A geolokáció engedélyezése problémás lehet a mobil Firefoxban. Lehet, hogy érdemes más böngészőt használni.");
    }
}

</script>

</body>
