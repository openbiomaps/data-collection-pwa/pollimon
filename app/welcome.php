<?php

?>

<div x-data="welcome" x-init="showWelcome()" x-show="welcomeopen || welcomeOpen" class="welcome-div">
  <div style="background: white; color: #3a3a3a; padding: 20px; border-radius: 10px;">
    <h2>Üdvözlünk a <i>Polli-mon Go!</i> alkalmazásban!</h2>
    <p>Ezzel az alkalmazással a beporzó állatokról tudsz adatokat gyűjteni a pollinator-monitoring.hu projekt keretében.
        <br>
        <br>
       Az alkalmazás használatához be kell jelentkezzél a projektbe, amit megtehetesz Google fiók segítségével, vagy
       <a href='https://openbiomaps.org/projects/pollimon/?registration' target='blank'>kérhetsz meghívót a projekt térképi adatbázis oldalán</a>.
        <br>
        <br>
       Ez az alkalmazás helyadatokat használ a mintavételi hely egyértelmű azonosításához, így az alkalmazás használatához engedélyt kell adnod az alkalmazás számára a helyadatokhoz való hozzáféréshez.<br>
       Ne felejtsd el bekapcsolni a Geolokalizációt (GPS) a telefonodon!
    </p>
    <button id="startGeoWatch" @click="close(), welcomeOpen = false, geoLocationEnabled = true" class="pure-button button-success">Rendben</button>
  </div>
</div>

