<?php

?>

<a 
    :href="$store.access_token_state ? '?logout' : 'https://<?php echo URL ?>/?weblogin&callback=https://<?php echo URL ?>/<?php echo basename(APP_PATH) ?>/index.php&scope=get_profile+put_data&cookie_path=<?php echo APP_PATH ?>'" 
    @click="$store.access_token_state ? logout() : 'https://<?php echo URL ?>/?weblogin&callback=https://<?php echo URL ?>/<?php echo basename(APP_PATH) ?>/index.php&scope=get_profile+put_data&cookie_path=<?php echo APP_PATH ?>'" 
    class="login_button"
>
    <i :class="{'material-icons': true}" style="font-size:24px;vertical-align:bottom" x-text="$store.access_token_state ? 'logout' : 'login'"></i>
    &nbsp;
    <span x-text="$store.access_token_state ? 'Bejelentkezve' : 'Nincs bejelentkezve'"></span>
</a>