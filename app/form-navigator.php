<?php

?>

<!-- Navigator -->
    <div class="tab-navigator" x-data="{ 
        activeTab: 0,
    }">
      <div class="scroll-container">
          <div class="tab-navi scroll-content">
            <div class="scroll-item active"
              @click="activeTab = 0"
              class="tab-control"
              :class="{ 'active': activeTab === 0 }"
            >Mintavételi hely</div>
            <div class="scroll-item"
              @click="activeTab = 1; if (activeTab === 1 && !running) { startTimer(); }"
              class="tab-control"
              :class="{ 'active': activeTab === 1 }"
            >Beporzók</div> 
            <div class="scroll-item"
              @click="activeTab = 2"
              class="tab-control"
              :class="{ 'active': activeTab === 2 }"
            >Növényzet és virágok</div>
            <div class="scroll-item"
              @click="activeTab = 3"
              class="tab-control"
              :class="{ 'active': activeTab === 3 }"
            >Időjárás</div>
          </div>  
          <div class="scroll-arrows">
            <div class="arrow left"><i class="fa fa-chevron-left" style="color:#fff;padding-top:.4rem"></i></div>
            <div class="arrow right"><i class="fa fa-chevron-right" style="color:#fff;padding-top:.4rem"></i></div>
          </div>
      </div>
      <!-- Progress bar -->
      <div class="progress-bar" x-bind:style="{width: ((activeTab + 1) / 4 * 100) + '%'}"></div>
      
      <!-- First tab -->
      <!-- Mintavételi hely -->
      <div class="tab-panel" :class="{ 'active': activeTab === 0 }" x-show.transition.in.opacity.duration.600="activeTab === 0" x-ref="tab1">
        <div x-data="newplotComponent()" x-init="getObserver(),getStreet()">
            <form id="samplinglocation-form" action="" class="pure-form panel-form" x-ref="form1">

                <div x-show="selected_sample_plot" 
                     x-data
                     x-init="$watch('$store.x.sites', () => $store.x.selectedSite = Object.keys($store.x.sites)[0])">
                    <div style="padding:0.5rem;text-align:center;border-bottom:1px solid white;max-width:30rem">Mintavételi hely kiválasztása.<br>A térképi pozíció alapján automatikusan kérdezli le az alkalmazás a közeli mintavételi pontokat. Amennyiben nem töltődnek be mintavételi hely nevek, próbálj meg a térképpel magtalálni a pozíciódat!</div>

                    <br>
                    <a href='' @click.prevent="new_sample_plot = !new_sample_plot;selected_sample_plot = !selected_sample_plot" class='pure-button button-secondary'>Új mintavétel hely felvétele</a>

                    <div class="label">Mintavételi hely neve</div>
                    <select id='site_name' class="" name="site_name" required 
                        x-model="$store.x.selectedSite" >
                        <!--x-on:change="$store.x.selectedSite == 'k0' ? setTab(0) : setTab(1)">-->
                      <template x-for="(value, key) in $store.x.sites">
                         <option :value="key" x-text="$store.x.sites[key].name"></option>
                      </template>
                    </select> 
                    <div class="label">Megfigyelő</div>
                    <input id='observer' name="observer" placeholder="megfigyelő neve" 
                        x-model="observer" required autocomplete="off"> 
                    <input id='observer_email' name="observer_email" placeholder="megfigyelő email címe" 
                        x-model="observer_email" required autocomplete="off"> 

                    <div class="label">GPS pozíció</div>
                    <!--<input id="gps_pos" name="gps_pos" x-model="gps_pos" readonly>-->
                    <div x-data="geoComponent('gps_pos')" x-init="init" x-on:unload="destroy">
                        <input id="gps_pos" type="text" readonly>
                    </div>
                </div>
            </form>

            <form id="new_samplinglocation-form" action="" class="pure-form panel-form" x-ref="form2">
                <div id='new_sample_plot_div' x-show="new_sample_plot">
                    <div style="padding:0.5rem;text-align:center;border-bottom:1px solid white;max-width:30rem">Mintavétel új helyen. 
                        <a href='https://pollinator-monitoring.hu/protocoll' target="_blank"><i class="material-icons" style="font-size:24px;vertical-align:bottom">help</i></a></div>

                    <br>

                    <a href='' @click.prevent="new_sample_plot = !new_sample_plot;selected_sample_plot = !selected_sample_plot" class='pure-button button-secondary'>Mintavétel állandó mintavételi helyen</a>
                    
                    <div class="label">Mintavételi hely neve</div>
                    <input class="pure-u-1" type="text" placeholder="Adj egy nevet a helynek" name="newplotName" id="newplotName" x-model="streetName" readonly required autocomplete="off">
                    
                    <div class="label">Megfigyelő</div>
                    <input id='new_observer' name="new_observer" placeholder="megfigyelő neve" 
                        x-model="observer" required autocomplete="off"> 
                    <input id='new_observer_email' name="new_observer_email" placeholder="megfigyelő email címe" 
                        x-model="observer_email" required autocomplete="off"> 

                    <div class="label">Élőhelytípus megadása</div>
                    <select class="pure-u-1 dropdown-menu-js" id="habitatType" name="habitatType" x-model="selectedHabitat" x-on:change="selectedLine=selectedHabitat" required>
                      <option value="" disabled selected>Válasszon egy élőhelytípust...</option>
                      <template x-for="(name, type) in habitatList" :key="type">
                          <option :value="type" x-text="name"></option>
                      </template>
                    </select>
                    
                    <div class="label">Készítsen egy fényképet felülről a mintavételi helyről (1m<sup>2</sup>)</div>
                    <nobr><i class="material-icons" style="font-size:24px">camera</i>&nbsp;<input type="file" id="photoInput" name="photoInput" accept="image/*" capture="camera" class="pure-u-7-8" multiple /></nobr>
                    <div id="photoContainer"></div>

                    <div class="label">Magánterületen van ez hely?</div>
                    <select class="pure-u-1" id="plotLocationType" name="plotLocationType" required>
                        <option value="" disabled selected>Válaszd ki</option><option value="private">magánterület</option><option value="public">közterület</option>
                    </select>

                    <div class="label">Alkalmi vagy állandó mintavételi hely?</div>
                    <select class="pure-u-1" id="plotType" name="plotType" required>
                        <option value="" disabled selected>Válaszd ki</option><option value="occasional">alkalmi</option><option value="permanent">állandó</option>
                    </select>
                    
                    <div class="label">GPS pozíció</div>
                    <!--<input id="new_gps_pos" name="gps_pos" x-model="gps_pos">-->
                    <div x-data="geoComponent('new_gps_pos')" x-init="init" x-on:unload="destroy">
                        <input id="new_gps_pos" type="text" readonly>
                    </div>
                </div>

            </form>
        </div>
      </div>

      <!-- Second tab -->
      <!-- Beporzók -->
      <div class="tab-panel" :class="{ 'active': activeTab === 1 }" x-show.transition.in.opacity.duration.600="activeTab === 1" x-ref="tab2">
        <div x-data="{ 
            selectedSpecies: '',
            selectedLine: '',
            speciesList: speciesList,
            newSpeciesInput: {},
            form: 'insects',
            init() {
                this.$watch('selectedSpecies', (value) => {
                    if (value) {
                        updateSpeciesCount(this.speciesCount, value, 1);
                        if (value !== 'user_input') {
                            this.newSpeciesInput[value] = this.speciesList[value] + ' / ' + (this.speciesCount[value].count || 1);
                        } else {
                            this.newSpeciesInput[value] = '';
                        }
                    }
                });
            }
        }">
            <form id="insects-form" action="" class="pure-form panel-form" x-ref="form3">
                <div style="padding:0.5rem;text-align:center;border-bottom:1px solid white;max-width:30rem">
                    Beporzók megfigyelési adatai. Amennyiben nem látsz beporzókat a megfigyelési területen nem kell kitölteni! A megfigyelési idő 5 perc!
                    <div style="padding:0.2em;background-color:#ff5757">
                        <div x-data="{ 
                            }" x-init="">
                            <template x-if="!expired">
                                <div x-text="'A hátralévő idő ' + timeLeft + ' másodperc'"></div>
                            </template>
                            <div x-show="expired" x-html="'Az idő lejárt!<br>Végezd a növényzet felmérését és küldd be az adatokat'"></div>
                        </div>
                    </div>
                </div>

                <div class="label">Beporzó fajok megadása 
                            <a href='https://pollinator-monitoring.hu/pollinators' target="_blank"><i class="material-icons" style="font-size:24px;vertical-align:bottom">help</i></a></div>

                <select class="pure-u-1 dropdown-menu-js" id="insects" name="insects" x-model="selectedSpecies" x-on:change="selectedLine=selectedSpecies" x-bind:disabled="expired">
                  <option value="" disabled selected>Válasszon egy fajt...</option>
                  <template x-for="(name, species) in speciesList" :key="species">
                      <option :value="species" x-text="name"></option>
                  </template>
                </select>

                <template x-for="(speciesData, species) in speciesCount" :key="species">
                    <div :id="'species_line_' + species" class="counter-element">
                        <button x-on:click.prevent="decreaseCount(speciesCount,newSpeciesInput,species, speciesList)" x-bind:disabled="expired">&nbsp; -  &nbsp;</button>
                        <input
                        x-bind:disabled="expired"
                        type="text"
                        :id="'newSpecies_' + species"
                        x-model="newSpeciesInput[species]"
                        x-cloak
                        :data-value="species"
                        :data-count="speciesData.count"
                        :placeholder="selectedSpecies === 'user_input' ? 'Írd be a fajnevet' : ''"
                        class="input-counter"
                        x-on:blur="addNewSpecies(newSpeciesInput, speciesCount, species, $event, speciesList)"
                        />
                        <button x-on:click.prevent="incrementCount(speciesCount,newSpeciesInput,species, speciesList)" x-bind:disabled="expired">&nbsp; + &nbsp;</button>
                    </div>
                </template>
            </form>
        </div>
      </div>

      <!-- Third tab -->
      <!-- Virágok -->
      <div class="tab-panel" :class="{ 'active': activeTab === 2 }" x-show.transition.in.opacity.duration.600="activeTab === 2" x-ref="tab3">
        <div>
            <form id="plants-form" action="" class="pure-form panel-form" x-ref="form4">
                <div style="padding:0.5rem;text-align:center;border-bottom:1px solid white;max-width:30rem">Növényzet állapotára vonatkozó információk. Kötelező mindet kitölteni!</div>

                <div class="label">Növényzet átlagos magassága (cm)</div>
                <div>
                    <input type="number" name="vegetationHeight" id="vegetationHeight" min="0" max="300" class="pure-u-1" required autocomplete="off">
               </div> 
                <div class="label">Zöld növényzet borítás (%)</div>
                <select id="plantCover" name="plantCover" class="pure-u-1" required>
                    <option value="" disabled selected>Zöld növényzet borítás (%)</option>
                    <option value="0-50">0-50%</option>
                    <option value="50-75">50-75%</option>
                    <option value="75-100">75-100%</option>
                </select>

                <div class="label">Virágok fajszáma (db)</div>
                <input type="number" name="flowerSpecies" id="flowerSpecies" min="0" max="30" class="pure-u-1" required autocomplete="off">

                <div class="label">Virágegységek száma (db)</div>
                <input type="number" name="flowerNumber" id="flowerNumber" min="0" max="300" class="pure-u-1" required autocomplete="off">

            </form>
        </div>
      </div>

      <!-- Forth tab -->
      <!-- Környezeti állapot -->
      <div class="tab-panel" :class="{ 'active': activeTab === 3 }" x-show.transition.in.opacity.duration.600="activeTab === 3" x-ref="tab4">
        <form id='enviroment-form' action="" class="pure-form panel-form" x-ref="form5">

            <div style="padding:0.5rem 0 0.5rem 0;text-align:center;border-bottom:1px solid white;max-width:30rem">Környezeti állapotokra vonatkozó adatok. Amennyiben van internet kapcsolatod és GPS hozzáférésed, akkor automatikusan kitöltésre kerül.<br>Ha nincsenek automatikusan beírt értékek, vagy nem tűnnek megfelelőnek, add meg őket.</div>
            <div x-data="{
                    weather: '',
                    async getWeather() {
                        this.weather = await retrieveWeather(localStorage.getItem('lon'),localStorage.getItem('lat'));
                    },
                    getCurrentDateTime() {
                        const now = new Date();
                        return formatDatetime(now);
                    }
                }"
                x-init="getWeather">

                <div class="label">Szél</div>
                <select class="pure-u-1 dropdown-menu-js plant-options" id="wind" name="wind" x-model="weather.wind" required>
                    <option value="" disabled selected>Adja meg a szél erősséget...</option>
                    <template x-for="option in windOptions" :key="option">
                        <option :value="option" x-text="option" :selected="convertWindCategory(weather.wind) === option"></option>
                    </template>
                </select>

                <div class="label">Hőmérséklet</div>
                <input type="text" name="temperature" id="temperature" class="pure-u-1" placeholder="Adja meg a hőmérsékletet" x-model="weather.temp" required autocomplete="off">

                <div class="label">Árnyék</div>
                <select class="pure-u-1 dropdown-menu-js plant-options" id="shadow" name="shadow" x-model="weather.clouds" required>
                    <option value="" disabled selected>Adja meg az árnyékoltság mértékét...</option>
                    <template x-for="option in shadowOptions" :key="option">
                        <option :value="option" x-text="option" :selected="convertShadowCategory(weather.clouds) === option"></option>
                    </template>
                </select>

                <div class="label">Megfigyelés időpontja</div>
                <input type="datetime-local" name="date" id="date" x-model="getCurrentDateTime()" style="width:100%" min="2024-01-01T00:01" required autocomplete="off">
            </div>
        </form>
        <button x-on:click.prevent="form_check()" class="pure-button button-success form-submit"> 
                <span id="buttonText">Adatok beküldése</span>
                <span id="loadingIcon" class="material-icons" style="display: none;">autorenew</span> </button>
        <!--<button @click="form_check()">Ellenőrzés</button>-->
      </div> <!-- tab-panel -->
    </div> <!-- tab-navigator -->
