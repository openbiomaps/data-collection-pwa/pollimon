<?php

?>

<div id="app-div" class="app-div" x-data="{
    new_sample_plot: false,
    selected_sample_plot: true,
    speciesCount: {},
    form_check: function() { this.do_form_check() },
    do_form_check: async function() {

        let names = {
            observer_email: 'Megfigylő email címe',
            new_observer_email: 'Megfigylő email címe',
            observer: 'Megfigylő neve',
            new_observer: 'Megfigylő neve',
            habitatType: 'Élőhelytípus',
            plotLocationType: 'Terület jellege',
            vegetationHeight: 'Növényzet átlagos magassága',
            plantCover: 'Zöld növényzet borítás',
            flowerSpecies: 'Virágok fajszáma',
            flowerNumber: 'Virágegységek száma',
            temperature: 'Hőmérséklet',
            wind: 'Szél',
            shadow: 'Árnyék',
            date: 'Megfigyelés időpontja'
        };

        let errors = [];
        if (this.new_sample_plot) {
            var formElements = document.getElementById('new_samplinglocation-form');
            errors = errors.concat( form_elements_check(formElements));
        } else {
            var formElements = document.getElementById('samplinglocation-form');
            errors = errors.concat( form_elements_check(formElements));
        }
        var formElements = document.getElementById('insects-form');
        errors = errors.concat( form_elements_check(formElements));
        var formElements = document.getElementById('plants-form');
        errors = errors.concat( form_elements_check(formElements));
        var formElements = document.getElementById('enviroment-form');
        errors = errors.concat( form_elements_check(formElements));

        if (errors.length == 0) {
            var results = await form_submit(this.speciesCount,this.new_sample_plot);

            if (
                (results.new_plot == null || results.new_plot == true) &&
                (results.pollinators == true) &&
                (results.vegetation == true)) {
                // Do some form cleaning..
                window.location.reload();
            }
        } else {
            for (let e=0;e<errors.length;e++) {
                let id = errors[e];
                document.getElementById(id).style.backgroundColor = 'red';
                alert(`Egy kötelező mező nincs kitöltve: ${names[id]}`);
            }
        }
    },
    timeLeft: 300,
    expired: false,
    running: false,
    startTimer: function() {
        if (this.running) {
            return;
        }
        this.running = true;
        var interval = setInterval(function () {
            if (this.timeLeft > 0) {
                this.timeLeft--;
            } else {
                this.expired = true;
                this.running = false;
                clearInterval(interval);
            }
        }.bind(this), 1000);
    }
 }"  x-mount="form_check">

