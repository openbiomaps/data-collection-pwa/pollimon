<?php
?>

<!-- Alpine x-data ... components 
<div id="app-div" class="app-div" .... -->
<?php require_once "app/app-div_definition.php"; ?>

<?php require_once "app/welcome.php"; ?>

<!-- form-container -->
<div class="form-container resizer" id="form-container" x-show="openForm">

    <!-- Main app spcific form contents -->
    <?php require_once "app/form-navigator.php"; ?>

    <!-- Log In Button -->
    <?php require_once "app/login_button.php"; ?>

</div> <!-- /form-Container -->

</div> <!-- /app div -->


<!-- The bottom banner is defined in the index.php -->
