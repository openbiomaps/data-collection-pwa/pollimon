<?php
# An OpenBioMaps API client application
# @Miklós Bán
# 2023-01-01

require_once('settings.php.inc');
require_once('functions.php');
require_once('ajax.php');

if (isset($_GET['logout'])) {
    $past = time() - 3600;
    foreach ( $_COOKIE as $key => $value )
    {
        setcookie( $key, $value, $past, '/' );
        setcookie( $key, $value, $past, APP_PATH );
    }
}
// returning from Google Sign In
if (isset($_POST['credential'])) {
    //debugx('Google Call');
    define('PROJECT_DIR',basename(__DIR__));
    require_once '../includes/vendor/autoload.php';
    // Google redirect
    $client = new Google_Client(['client_id' => $_POST['client_id']]);  // a kliensazonosító beállítása
    $payload = $client->verifyIdToken($_POST['credential']);  // az ID Token ellenőrzése

    if ($payload) {
        $scopeRequired = "get_profile put_data";
        $client = CLIENT_ID;
        $client_secret = CLIENT_SECRET;
        // a felhasználó hitelesítve van, készítünk egy access_token-t neki
        require_once '../oauth/server.php';
        require_once '../oauth/google.php';

        //debugx($token,__FILE__,__LINE__);
        // beállítjuk a süti-be a tokeneket
        if (isset($token['access_token'])) {
            setcookie("access_token", $token['access_token']);
            setcookie("refresh_token", $token['refresh_token']);
        } else {
            // Log In to OBM failed
        }
    } else {
        // Log In to Google failed
        // érvénytelen ID Token
        //echo json_encode(array("error"=>"Invalid ID Token"));
        //die;
    }
    unset($_POST['credential']);
}

?>
<!doctype html>
<html lang="en">
<head>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CODELAB: Add link rel manifest -->
  <link rel="manifest" href="manifest.json?rev=<?php echo revx('manifest.json'); ?>">
<!-- CODELAB: Add iOS meta tags and icons -->
<!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="Polli-mon Go!">
  <link rel="apple-touch-icon" href="images/icons/android/android-launchericon-144-144.png">
  <link rel="icon" href="https://openbiomaps.org/img/favicon.ico" type="image/x-icon" />
<!-- description -->
<meta name="description" content="<?php echo DESCRIPTION ?>">
<!-- meta theme-color -->
  <meta name="theme-color" content="#aad2dd" />

  <link rel="stylesheet" href="https://unpkg.com/purecss@2.1.0/build/pure-min.css" integrity="sha384-yHIFVG6ClnONEA5yB5DJXfW2/KC173DIQrYoZMEtBvGzmf0PKiGyNEqe9N6BNDBH" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css" href="styles/fontawesome-free-6.1.1-web/css/fontawesome.min.css">
  <link rel="stylesheet" type="text/css" href="styles/fontawesome-free-6.1.1-web/css/solid.min.css">
  <link rel="stylesheet" type="text/css" href="styles/inline.css?rev=<?php echo revx('styles/inline.css'); ?>">

<!-- App spcific style files -->
<?php
require "app/appcss.php";
?>
  <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
  <!-- Local stroge -->
  <script src="scripts/localforage.js"></script>
  <!-- Keep screen on -->
  <script src="scripts/NoSleep.min.js"></script>
  <!-- OpenLayers -->
  <script src="https://cdn.jsdelivr.net/npm/ol@v8.2.0/dist/ol.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ol@v8.2.0/ol.css">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <!-- Alpine JS -->
  <!--<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>-->
  <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
  <!-- Crypto JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>
    
  <title><?php echo TITLE ?></title>
</head>


<?php

// Load GeoLocation permission query app
if (!isset($_GET['geolocation'])) {
    require('geolocation.php');
    exit;
}
?>

<body x-data="{ 
<?php
// Create app/body_x-data_definition.php overwrite the default body's x-data
require_check('body_x-data_definition.php');
?>
}">
<!-- PWA WakeLock -->
<script src="scripts/wakelock.js?revx=<?php echo revx('scripts/wakelock.js'); ?>"></script>
<!-- constants -->
<script type="text/javascript">
    const URL = '<?php echo URL ?>';
    const COOKIE_PATH = '<?php echo APP_PATH ?>';
    const APP_PATH = '<?php echo basename(APP_PATH) ?>';
    const API_VERSION = '<?php echo API_VERSION ?>';
    const CLIENT_ID = '<?php echo CLIENT_ID ?>';
    const CLIENT_SECRET = '<?php echo CLIENT_SECRET ?>';
</script>
<!-- authentication -->
<script src="scripts/auth.js?revx=<?php echo revx('scripts/auth.js'); ?>"></script>

<script>
// Alpine Init
document.addEventListener('alpine:init', () => {

<?php
// Default Alpine init variables and functions
require_once "main/Alpine.store.init.php";

// Add your Alpine init variables and functions
require_once "app/Alpine.store.init.php";
?>

});
</script>



<!-- OpenLayers map div -->
<div id="map" class="map"></div>

<!-- Add your project specific custom javascript files -->
<?php
require_check('appjs.php');
?>

<div class="main-div">

<?php
/* AlpineJS .app-div the forms or something else
 *
 * */
require_once('app.php');

?>

<!-- Info modal -->
<?php
// Create app/info.php to overwrite the default info modal
require_check("info.php");
?>

<!-- GPS data dialog -->
<?php
// Create app/GPSbox.php to overwrite the default GPSbox modal
require_check("GPSbox.php");
?>

<!-- footer banner -->
<?php
// Create app/footer.php to overwrite the default footer banner
require_check ("footer.php");
?>

</div> <!-- main div -->

<!-- Genaral javascript functions -->
<script src="scripts/functions.js?rev=<?php echo revx('scripts/functions.js'); ?>"></script>

<!-- Map related javascript functions -->
<script type="text/javascript">
    /* Cluster layer 
     * This is used when we are fetching spatial data  from the server and display on map
     * */
    const distanceInput = <?php echo $distanceInput ?>;
    const minDistanceInput = <?php echo $minDistanceInput ?>;
    const min_zoom_for_filter = <?php echo $min_zoom_for_filter ?>;
    const target_table = '<?php echo isset($_GET['table']) ? $_GET['table'] : PROJECTTABLE ?>';
    const wms_cluster = '<?php echo $wms_cluster ?>';
</script>
<!-- Init OpenLayers objects -->
<script src="scripts/map_init.js?rev=<?php echo revx('scripts/map_init.js'); ?>"></script>
<!-- WMS clustered data layer fetching -->
<script src="scripts/map_wms_fetch.js?rev=<?php echo revx('scripts/map_wms_fetch.js'); ?>"></script>
<!-- Load OpenLayers map & map based functions -->
<script src="scripts/map_load.js?rev=<?php echo revx('scripts/map_load.js'); ?>"></script>

<script>
    // <!-- Register service worker. --->
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('service-worker.js')
            .then((reg) => {
              console.log('Service worker registered.', reg);
            });
      });
    }
</script>    

<script>
/*document.addEventListener("DOMContentLoaded", function() {
  //var loader = document.getElementById("loader");
  var app = document.getElementById("app");
  var geoButton = document.getElementById("geoButton");

  //loader.style.display = "block";

  geoButton.addEventListener("click", function() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        console.log("Latitude: " + position.coords.latitude);
        console.log("Longitude: " + position.coords.longitude);
        //loader.style.display = "none";
        app.style.display = "block";
      }, function(error) {
        console.log("A felhasználó nem engedélyezte a GPS használatát.");
      });
    } else {
      console.log("A böngésző nem támogatja a Geolokációt.");
    }
  });
});*/
</script>
<script src="scripts/install.js"></script>

</body>
</html>
