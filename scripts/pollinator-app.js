const speciesList = {
    'user_input': 'Megadom a fajnevet',
    'hazi_meh': 'Házi méh',
    'poszmeh': 'Poszméh',
    'vadmeh': 'Egyéb vadméh',
    'zengolegy': 'Zengőlégy',
    'poszorlegy': 'Pöszörlégy',
    'legy': 'Egyéb légy',
    'lepke': 'Lepke',
    'darazs': 'Darázs',
    'fadongo': 'Fadongó',
    'bogar': 'Bogár',
    'nem_tudom': 'Nem tudom meghatározni'
}


const habitatList = {
    'grass': 'lágyszárú',
    'bush': 'bokros',
    'trees': 'fás'
}
const windOptions = [
    'nincs','gyenge szellő','enyhe szél','gyenge szél',
    'mérsékelt szél','erős szél'
];
const shadowOptions = ['nincs','részleges','teljes'];

function updateSpeciesCount(speciesCount, species, count) {
    if (speciesCount[species]) {
        speciesCount[species].count += count;
        //if (count) {
        //    speciesCount[species].position.push('1x1');
        //} else {
        //    speciesCount[species].position.splice(-1);
        //}
    } else {
        speciesCount[species] = {
            count:count,
        //    position:['1x1']
        };
    }
}
function addNewSpecies(newSpeciesInput, speciesCount, species, event, speciesList) {
    const newSpeciesName = event.target.value;
    if (newSpeciesName) {
        if (species === 'user_input') {
            speciesList[newSpeciesName] = newSpeciesName;
            speciesCount[newSpeciesName] = {
                count: 0,
            //    position: []
            }
            updateSpeciesCount(speciesCount, newSpeciesName, 1);
            delete speciesCount[species];
            newSpeciesInput[newSpeciesName] = newSpeciesName + ' / 1';
        }
    }
}
function incrementCount(speciesCount, newSpeciesInput, species, speciesList) {
    updateSpeciesCount(speciesCount, species, 1);
    if (newSpeciesInput[species]) {
        newSpeciesInput[species] = speciesList[species] + ' / ' + speciesCount[species].count;

    }
}

function decreaseCount(speciesCount, newSpeciesInput, species,speciesList) {
    if (speciesCount[species].count > 0) {
        updateSpeciesCount(speciesCount, species, -1);
        if (speciesCount[species].count === 0) {
            delete speciesCount[species];
        }
        else if (newSpeciesInput[species]) {
            newSpeciesInput[species] = speciesList[species] + ' / ' + speciesCount[species].count;
        }
    }
}
// Új mintavételi hely Alpine komponens készítése
function newplotComponent() {
    return {
        selectedHabitat: '',
        selectedLine: '',
        habitatList: habitatList,
        streetName: '',
        observer: '',
        observer_email: '',
        // OpenStreetMap cím lekérdezés és mintavélei hely név generálás
        async getStreet() {
            getStreetName(localStorage.getItem('lon'),localStorage.getItem('lat'))
                .then(data => {
                    if (data && data.address) {
                        const streetName = data.address.road || random_string(8);
                        const postCode =  data.address.postcode || random_string(4);
                        const countryCode = data.address.country_code || random_string(2)
                    
                        this.streetName = gen_site_name(countryCode + postCode + streetName);
                    } else {
                        this.streetName = random_string(16) + "_" + random_string(5);
                    }
                }).catch (error => {
                    this.streetName = random_string(16) + "_" + random_string(5);
                });


        },
        // Megfigyelő nevének és email címének leszedése
        async getObserver() {
            let cookieValue = getCookie('access_token');

            if (cookieValue == '') {
                cookieValue = await refreshToken();
                if (cookieValue != '') {
                    Alpine.store('access_token_state',true);
                }
            } else {
                Alpine.store('access_token_state',true);
            }

            const ob = await getObserver();
            if (typeof ob !== 'undefined') {
                this.observer = ob.username;
                this.observer_email = ob.email;
            }
        }
    };
}

// Weather data transformation
function convertWindCategory(wind) {
        if (wind <= 2) {
            return 'nincs';
        } else if (wind <= 6) {
            return 'gyenge szellő';
        } else if (wind <= 11) {
            return 'enyhe szél';
        } else if (wind <= 19) {
            return 'gyenge szél';
        } else if (wind <= 29) {
            return 'mérsékelt szél';
        } else if (wind >= 30 ) {
            return 'erős szél';
        }
}
function convertShadowCategory(cloud) {
        if (cloud <= 10) {
            return 'nincs';
        } else if (cloud <= 85) {
            return 'részleges';
        } else if (cloud <= 100) {
            return 'teljes';
        } else {
            return '';
        }
}


// Form submit
// Itt még mindig vannak jquery hivatkozások, amit majd
async function form_submit(speciesCount, new_sample_plot) {
    
    var loadingIcon = document.getElementById("loadingIcon");
    var buttonText = document.getElementById("buttonText");
    
    buttonText.style.display = "none";
    loadingIcon.style.display = "block";

    const insects = speciesCount;
    let batch = [];

    const date = $("#date").val(); // Environment tab 

    // Site tab
    let site_name = $("#site_name").val();
    let observer = $("#observer").val();
    let observer_email = $("#observer_email").val();
    let management = ""; //$("#management").val();
    let gps_pos = $("#gps_pos").val();

    // Form beküldés eredményeinek objektuma
    // Három űrlapot is beküldünk!
    results = {
        new_plot: null,
        pollinators: null,
        vegetation: null
    };

    // New site tab
    if (new_sample_plot) {
        const habitatType = $("#habitatType").val();  // Élőhelytípus: lágyszárú, bokros, fás

        const fileInput = $("#photoInput");

        //const photos = fileInput[0].files.map((image, index) => `file${index}`);
        const photos = [];
        Object.keys(fileInput[0].files).forEach(function(key, index) {
            photos.push(`file${index}`);
        });
        
        const locationType = $("#plotLocationType").val(); // közterület, magán
        const plotType = $("#plotType").val(); // alkalmi, állandó
        site_name = $("#newplotName").val();
        observer = $("#new_observer").val();
        observer_email = $("#new_observer_email").val();
        management = ""; //$("#new_management").val();
        gps_pos = $("#new_gps_pos").val();

        // New site form
        batch.push(
          {
            data: [{
                "date": date,
                "observer" : observer,
                "observer_email" : observer_email,
                "name" : site_name,
                "location_type": locationType,
                "plot_type": plotType,
                "habitat_type": habitatType,
                "obm_geometry": gps_pos,
            }],
            attached_files: photos.join(','),
          });

        await form_send(1279, batch, fileInput[0].files).then(result => {
            results.new_plot = result;
        });

        // 1038
    }

    // Observations
    // Environment tab
    let wind = $("#wind").val();
    let temperature = $("#temperature").val();
    let shadow = $("#shadow").val();
    batch = [];

    // Obsevation id
    let observation_id = generateUniqueHash(date + site_name + observer);

    // Pollinator form data
    let pollinators = {};
    let n = 0;
    for (let key in insects) {
        if (insects.hasOwnProperty(key)) {
            let insect = insects[key];
            let count = insect.count;
            let species = key;
            //let positions = insect.position;
            for (let i=0;i<count;i++) {
                pollinators[n] = {
                    "species_category" : species, // predefined categories
                    "species" : "", // other
                    "count" : 1,
                    //"position" : positions[i] || '1x1',
                }
                n++;
            }
        }
    }
    let post = [];
    let keys = Object.keys(pollinators);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        let p = pollinators[key];

        let record = {
            "date": date,
            "observer" : observer,
            "observer_email" : observer_email,
            "observation_id": observation_id,
            "location_name" : site_name,
            "shadow": shadow,
            "wind": wind,
            "temperature": temperature,
            "species" : p.species,
            "species_category": p.species_category,
            "noindividuals": p.count, // 1
            "obm_geometry": gps_pos,
        }
        post.push(record);
    }
    if (post.length == 0) {
        // Creating NULL Record'
        let record = {
            "date": date,
            "observer" : observer,
            "observer_email" : observer_email,
            "observation_id": observation_id,
            "location_name" : site_name,
            "shadow": shadow,
            "wind": wind,
            "temperature": temperature,
            "species" : "",
            "species_category": "",
            "noindividuals": 0, // 1
            "obm_geometry": gps_pos,
        }
        post.push(record);
    }
    batch.push(
      {
        data: post,
      });

    await form_send(1254, batch, []).then(result => {
        results.pollinators = result;
    });


    // Plant data
    let plant_cover = $("#plantCover").val();
    plant_cover = plant_cover ? plant_cover : '';
    batch = [];
    record = {
        "date": date,
        "observer": observer,
        "observer_email" : observer_email,
        "observation_id": observation_id,
        "location_name": site_name,
        "vegetation_height": $("#vegetationHeight").val(),
        "plant_cover": plant_cover,
        "flower_count": $("#flowerNumber").val(),
        "plant_num": $("#flowerSpecies").val(),
        //"obm_geometry": "",
        //"obm_files_id": "",
        //"field_state": fstate,
    }
    batch.push(
    {
        data: [record],
    });
    
    await form_send(1258, batch, []).then(result => {
        results.vegetation = result;
    });

    // resetting
    //speciesCount = {};
    //plantCount = {};

    //{ new_plot: null, pollinators: true, vegetation: true }
    if ((results.new_plot == null || results.new_plot == true) &&
        (results.pollinators == true) &&
        (results.vegetation == true)) {

        alert("Adatok elküldve!");
    } else {
        alert("Az adatok beküldése közben valami hiba történt!");
    }
    buttonText.style.display = "inline";
    loadingIcon.style.display = "none";
    
    // Visszatér az App.php Alpine do_form_check() függvényébe 
    return results;
}

// This function should be exists even if it empty!
// Trigger function with WGS84 coordinates to execute some project specific functions
async function custom_locationTrigger(longitude,latitide) {

    // streetName
    getStreetName(longitude,latitide).then(data => {
        const streetName = data.address.road || random_string(8);
        const postCode =  data.address.postcode || random_string(4);
        const countryCode = data.address.country_code || random_string(2)
        
        document.getElementById('newplotName').value = gen_site_name(countryCode + postCode + streetName);
    }).catch (error => {
        document.getElementById('newplotName').value = random_string(16) + "_" + random_string(5);
    });

    // weather
    retrieveWeather(longitude,latitide).then(weather => {
        document.getElementById('wind').value = convertWindCategory(weather.wind);
        document.getElementById('shadow').value = convertShadowCategory(weather.clouds);
        document.getElementById('temperature').value = weather.temp;
    });

}


// This function should be exists even if it empty!
async function custom_bboxTrigger(bbox) { 

    obm_query_geom_selection('pollimon_sample_plots', bbox)
        .then(response => {
            let plots = {};
            let features = new Array;
            for (let k=0;k<response.length;k++) {
                if (response[k].plot_type == 'occasional') {
                    continue;
                }
                let feature = new ol.format.WKT().readFeatures(response[k].obm_geometry,{
                    'dataProjection': "EPSG:4326",
                    'featureProjection': "EPSG:3857"});
                feature[0].setProperties(response[k]);
                features.push(feature[0]);

                plots["k" + response[k].obm_id] = {
                    'observer': response[k].observer,
                    'location_type': response[k].location_type,
                    'name': response[k].name,
                    'environment': response[k].environment,
                    'obm_files_id': response[k].obm_files_id
                  }
            }
            
            // frissítjük az Alpine's store-t az új értékekkel
            Alpine.store('x').updateSites(plots);
            
            // Cluster
            clusterSource.getSource().clear(true);
            clusters.setVisible(true);
            clusterSource.getSource().addFeatures(features);
        })
        .catch(error => {
            console.error('An error occurred:', error)

            Alpine.store('sites',{});
            setTab(0);
        });
}
