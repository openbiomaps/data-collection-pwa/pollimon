/* 
 *
 *
 *
 * */

// Set localforage
localforage.setDriver([
    localforage.INDEXEDDB,
    localforage.WEBSQL,
    localforage.LOCALSTORAGE
]);

function isJSON(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exhours, path = '/') {
    const d = new Date();
    d.setTime(d.getTime() + ((exhours+3)*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    let cookie = {
        "expiry":d.setTime(d.getTime() + (exhours*60*60*1000)),
        "data":{}
    }
    cookie["data"][cname] = cvalue;
    document.cookie = cname + "=" + JSON.stringify(cookie) + ";" + expires + ";path=" + path;
}

async function getRefreshToken() {
    try {
        let refresh_token = await localforage.getItem('refresh_token');
        return refresh_token;
    } catch(err) {
        console.log('refresh token reading failed from localforage');
    }
}
async function refreshToken() {

    let cookie_path = COOKIE_PATH;
    //let refresh_token = getCookie("refresh_token");
    let refresh_token = await getRefreshToken();

    try {
        c = JSON.parse(refresh_token);
        refresh_token = c.data.refresh_token;
        cookie_path = '/'; // A Google JSON refresh token-t csinál és a path a /
    } catch {
        //console.log("Refresh token is not JSON:" + refresh_token);
    } 

    if (refresh_token == "") {
        let access_token = getCookie("access_token");
        if (isJSON(access_token)) {
            c = JSON.parse(access_token);
            access_token = c.data.access_token;
        } 
        if (access_token != "") {
            setCookie("access_token", access_token, -4, COOKIE_PATH); // trying to delete cookie
            setCookie("access_token", access_token, -4, "/"); // trying to delete cookie
            Alpine.store('access_token_state',false);
        }
        //alert ("Log in again!")
        return false;
    }

    const url = "https://" + URL + "/oauth/token.php";
    const headers = {
        "Authorization": "Basic " + btoa(CLIENT_ID + ":" + CLIENT_SECRET),
        "Content-Type": "application/x-www-form-urlencoded"
    };
    const data = {
        grant_type: "refresh_token",
        refresh_token: refresh_token,
    };
    const body = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join("&");

    try {
        const response = await fetch(url, {
            method: "POST",
            headers: headers,
            body: body
        });

        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        let access_token = await response.json();
        //Alpine.store('access_token_state',true);
        if (isJSON(access_token)) {
            c = JSON.parse(access_token);
            access_token = c.data.access_token;
        } 
        else if (typeof access_token === 'object') {
            access_token = access_token.access_token;
        }

        localforage.setItem('refresh_token', refresh_token);
        setCookie("access_token", access_token, 1, cookie_path);
        return access_token;

    } catch (error) {
        //alert("Log in again!");
        console.error(`Fetch Error: ${error}`);

        let access_token = getCookie("access_token");
        if (isJSON(access_token)) {
            c = JSON.parse(access_token);
            access_token = c.data.access_token;
        } 

        if (access_token != "") {
            setCookie("access_token", access_token, -4, cookie_path); // trying to delete cookie
        }
        setCookie("refresh_token", refresh_token, -4, cookie_path); // trying to delete cookie
        //Alpine.store('access_token_state',false);
    }
    return false;
}

/*let refreshTokenPromise = null;

async function refreshToken() {
    if (!refreshTokenPromise) {
        refreshTokenPromise = (async () => {
            let cookie_path = COOKIE_PATH;
            let refresh_token = getCookie("refresh_token");
            try {
                c = JSON.parse(refresh_token);
                refresh_token = c.data.refresh_token;
                cookie_path = '/'; // A Google JSON refresh token-t csinál és a path a /
            } catch {
                //console.log("Refresh token is not JSON:" + refresh_token);
            } 

            if (refresh_token == "") {
                return false;
            }

            const url = "https://" + URL + "/oauth/token.php";
            const headers = {
                "Authorization": "Basic " + btoa(CLIENT_ID + ":" + CLIENT_SECRET),
                "Content-Type": "application/x-www-form-urlencoded"
            };
            const data = {
                grant_type: "refresh_token",
                refresh_token: refresh_token,
            };
            const body = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join("&");

            try {
                const response = await fetch(url, {
                    method: "POST",
                    headers: headers,
                    body: body
                });

                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }

                const jsonResponse = await response.json();
                setCookie("access_token", jsonResponse, 1, cookie_path);
                refreshTokenPromise = null;
                return jsonResponse;

            } catch (error) {
                alert("Log in again!");
                console.error(`Fetch Error: ${error}`);

                let access_token = getCookie("access_token");
                try {
                    c = JSON.parse(access_token);
                    access_token = c.data.access_token;
                } catch {
                    //console.log("Access token is not JSON");
                } 

                if (access_token != "") {
                    setCookie("access_token", access_token, -4, cookie_path); // trying to delete cookie
                }
                setCookie("refresh_token", refresh_token, -4, cookie_path); // trying to delete cookie
                refreshTokenPromise = null;
            }
            return false;
        })();
    }
    return refreshTokenPromise;
}
*/
