// For Firefox

let currentPosition = [localStorage.getItem('lon'),localStorage.getItem('lat')];

/*navigator.permissions.query({
    name: 'geolocation'
  }).then(function(result) {
      
    const onLocationFetchSuccess = (position) => {
        currentPosition = {
            lon:position.coords.longitude,
            lat:position.coords.latitude,
        };
    };

    const onLocationFetchFailure = (error = {}) => {
      // Error code 1 corresponds to user denying/blocking the location permission
      if (error.code === 1) {
        // Respond to failure case as required
        console.log('location access denied');
        alert('Engedélyezd a lokalizáció használatot az alkalmazás számára!');
      } else {
        console.log(error);
        alert('Be van kapcsolva a GPS a készülékeden?');
      }
    };

    navigator.geolocation.getCurrentPosition(onLocationFetchSuccess, onLocationFetchFailure);

    if (result.state === 'denied') {
      onLocationFetchFailure();
    }

    // This will still work for Chrome
    result.onchange = function() {
      if (result.state === 'denied') {
        onLocationFetchFailure();
      }
    }
});*/

// Creating ol.map object
const map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      }),
      //wmsLayer,
      clusters,
      drawLayer,
      markerLayer,
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([18.854118,47.458825]), // Budapest center
      projection: projection,
      zoom: 8
    })
});

// Inital set center and zoom
async function loadMap() {
    //currentPosition = await getPosition();
    if (typeof currentPosition !== 'undefined') {
        //map.getView().animate({center: ol.proj.fromLonLat([currentPosition.lon, currentPosition.lat])},{zoom: 18});
        map.getView().animate({center: ol.proj.fromLonLat([localStorage.getItem('lon'),localStorage.getItem('lat')])},{zoom: 18});
    }
}
loadMap();

/* OpenLayers GeoLocation API:
    - Track position:
        - Speed info
    - Accuracy info
    - Position info
*/
const geolocation = new ol.Geolocation({
    trackingOptions: {
        enableHighAccuracy: true,
    },
    projection: map.getView().getProjection(),
    tracking: true,
});

// Globális változó a térkép frissítésének állapotához
let updateMap = false;

// GeoLocation Events
// handle geolocation error.
geolocation.on('error', function (error) {
   console.log(error.message);
});

geolocation.on('change:accuracyGeometry', function () {
    accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
});

let trackline_start = 0;
geolocation.on('change:position', function () {

    const Position = geolocation.getPosition();

    // Updating the general currentPosition obj
    var wgs = ol.proj.transform(Position, 'EPSG:3857', 'EPSG:4326');
    currentPosition = {
        lon: wgs[0],
        lat: wgs[1],
    }
    positionFeature.setGeometry(Position ? new ol.geom.Point(Position) : null);

    if (updateMap) {
        var start_point = Position;
        var end_point = Position;

        if (!trackline_start) {
            tracklineSource.addFeatures([
                new ol.Feature(new ol.geom.LineString([start_point, end_point]))
            ]);
            trackline_start = 1;
        } else {
            let line = tracklineSource.getFeatures()[0].getGeometry();
            line.appendCoordinate(Position);
            map.getView().setCenter(Position);
        }
    }

});

// Térkép frissítésének be- és kikapcsolása
function toggleMapUpdate() {
    updateMap = !updateMap;
}

// GeoLocation Layer
var geolocationLayer = new ol.layer.Vector({
  map: map,
  source: new ol.source.Vector({
    features: [accuracyFeature, positionFeature],
  }),
});

/* Draw on map 
 * */
const modify = new ol.interaction.Modify({source: drawSource});
map.addInteraction(modify);
     
/* Map click event 
 * */
map.on('click', (e) => {
    const v = e.coordinate;

    markerSource.clear(true);
    
    // Turn off geolocation tracking  
    trackLocEnabled = locationTrack('off');

    // triggering custom functions with updated location info
    var wgs = ol.proj.transform(v, 'EPSG:3857', 'EPSG:4326');
    custom_locationTrigger(wgs[0],wgs[1]);

    fixed_position = v;

    const marker = new ol.Feature({
        geometry: new ol.geom.Point(v),
    });
    marker.setStyle(new ol.style.Style({
        image: new ol.style.Icon({
            anchor: [0.5, 1],
            src: 'images/marker.png', 
            size: [20,32],
        }),
    }));
    markerLayer.getSource().addFeature(marker);


    var actualZoom = map.getView().getZoom();
    let zoom;
    if (actualZoom < 18) {
        zoom = eval(actualZoom + 2)
    } else {
        zoom = actualZoom;
    }
    map.getView().animate({center: [v[0],v[1]]}, {zoom: zoom});
});

/* User's zoom action event end:
 * call data filter in BBOX
 * */
map.on('moveend', function(e) {
    filter();

});
    
/* Tracklogging
    - Trackline draw
    - Following geolocation position changes on map
 */
const tracklineSource = new ol.source.Vector();
const tracklineLayer = new ol.layer.Vector({
    source: tracklineSource,
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({
          color: [0,0,0,0.6],
          width: 2,
          lineDash: [4,8],
          lineDashOffset: 6
      }),
    }),
});
map.addLayer(tracklineLayer);

const track = document.createElement('div');
track.className = 'ol-control ol-unselectable track';
track.innerHTML = '<button id="trackloc" title="Turn off tracking"><i class="fa-solid fa-route" style=""></i></button>';
map.addControl(new ol.control.Control({
    element: track
}));

/* Track location control button
 * */
var toggleLoc = document.querySelector("#trackloc");
let trackLocEnabled = locationTrack('on');

toggleLoc.addEventListener('click', function() {
    if (!trackLocEnabled) {
      updateMap = true;
      trackLocEnabled = true;
      toggleLoc.title = "Location tracking is enabled";
      toggleLoc.querySelector('i').style.color = "lightskyblue";
    } else {
      updateMap = false;
      trackLocEnabled = false;
      toggleLoc.title = "Location tracking is disabled";
      toggleLoc.querySelector('i').style.color = "darkslategray";
    }
}, false);

/* User location 
 * Top-left corner's crosshair button
 * */
const locate = document.createElement('div');
locate.className = 'ol-control ol-unselectable locate';
locate.innerHTML = '<button title="Locate me"><i class="fa-solid fa-location-crosshairs"></button>';
locate.addEventListener('click', function() {
    //if (!accuracySource.isEmpty()) {
    let lon;
    let lat;
    if (!geolocationLayer.getSource().isEmpty()) {
        
        // Itt is kikapcsoljuk a trackelést
        trackLocEnabled = locationTrack('off');
        
        // kitöröljük a marker rétegről a marker ikont.
        markerLayer.getSource().clear();
        fixed_position = [];

        // triggering custom functions with updated location info
        console.log(currentPosition);
        var wgs = ol.proj.transform(currentPosition, 'EPSG:3857', 'EPSG:4326');
        custom_locationTrigger(wgs[0],wgs[1]);

        // Set default zoom
        map.getView().fit(geolocationLayer.getSource().getExtent(), {
            maxZoom: 18,
            duration: 500
        });
    }
});
map.addControl(new ol.control.Control({
    element: locate
}));

// Kikapcsoljuk a trackelést a térképi felbontás módosításakor
map.getView().on('change:resolution', function(event) {    
    trackLocEnabled = locationTrack('off');
});

/*
// Clear filter function
var clearF = function(e) {
    console.log('clear');
    wmsLayer.setVisible(true);
    clusters.setVisible(false);
    clusterSource.getSource().clear(true);
    drawSource.clear(true);
}*/

/* Query data from OBM based on BBOX
 * */
function filter(e) {

    var actualZoom = map.getView().getZoom();

    // Prevent fetching too much data..
    if (actualZoom < min_zoom_for_filter) {
        return;
    }

    let polygonFeature;
    let extent = map.getView().calculateExtent(map.getSize());
    let polygon = new ol.geom.Polygon.fromExtent(extent);
    polygon.scale(0.5, 0.5)
    features = [new ol.Feature(polygon)];
    //console.log(features[0].getGeometry().getCoordinates());

    drawLayer.getSource().clear();
    drawSource.addFeatures(features);
    polygonFeature = new ol.Feature(new ol.geom.Polygon(features[0].getGeometry().getCoordinates()));

    let format = new ol.format.WKT();
    let src = 'EPSG:3857';
    let dest = 'EPSG:4326';
    let wktRepresenation = format.writeGeometry(polygonFeature.getGeometry().clone().transform(src,dest));


    custom_bboxTrigger(wktRepresenation);
    
};
