// Screen lock control
/*var noSleep = new NoSleep();
noSleep.enable(); // keep the screen on!
var wakeLockEnabled = true;
var toggleEl = document.querySelector("#togglekeep");
toggleEl.addEventListener('click', function() {
    if (!wakeLockEnabled) {
          noSleep.enable(); // keep the screen on!
          wakeLockEnabled = true;
          toggleEl.title = "Wake Lock is enabled";
          toggleEl.style.color = "lightskyblue";
    } else {
          noSleep.disable(); // let the screen turn off.
          wakeLockEnabled = false;
          toggleEl.title = "Wake Lock is disabled";
          toggleEl.style.color = "darkslategray";
    }
}, false);*/
let wakeLock = null;

// Function that attempts to request a screen wake lock
const requestWakeLock = async () => {
  try {
    wakeLock = await navigator.wakeLock.request('screen');
    wakeLock.addEventListener('release', () => {
      console.log('Screen Wake Lock was released');
    });
    console.log('Screen Wake Lock active');
  } catch (err) {
    console.error(`${err.name}, ${err.message}`);
  }
};

// Request a screen wake lock…
requestWakeLock();

