
// An interface variable between OpenLayers and Alpine.js to store GPS position
let fixed_position = [];

//localStorage.setItem('lon', 18.854118);
//localStorage.setItem('lat', 47.458825);

function formatDatetime(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    
    return `${year}-${month}-${day} ${hours}:${minutes}`;
}

// Fetching available tables
async function get_tables() {

    const access_token = await refreshToken();
    $.ajax({
        type: "POST",
        url: 'https://' + URL + '/v'+ API_VERSION +'/pds.php',
        data: {
            access_token: access_token.access_token,
            scope: 'get_tables',
            table: target_table 
        },
        dataType: 'json',
        success: function (response) {
            tables = response['data'];
            tables.sort();
            let x = tables.map(function(v) {
                return $('<option/>', {
                  value: v,
                  text: v
                })
            });
            $('#table_list').append(x);
            $('#table_list').val(target_table);
                
         },
        error: function () {
            //alert("MAP connection error!");
        }
    });

}
// Using OBM query API
async function obm_query_geom_selection(target_table, bbox) {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        let url = 'https://' + URL + '/index.php?query&qtable=' + target_table + '&geom_selection=wktquery&geometry=' + bbox + '&output=json&filename=';
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                try {
                    let json = JSON.parse(xhr.responseText);
                    resolve(json);
                } catch (error) {
                    resolve({});
                }

            } else if (xhr.readyState === 4) {
                reject(xhr.status);
            }
        };
        xhr.send();
    });
}

// Form submit
// The `form_id` is a valid numeric id from the OpenBioMaps project: get_form_list
// The `post` is javascript object, key-value pairs 
async function form_send(form_id,post,images) {

    return new Promise(async (resolve, reject) => {

        const access_token = await refreshToken();
        const headers = [];
        for (const key in post[0].data[0]) {
            if (post[0].data[0].hasOwnProperty(key)) {
                if (typeof post[0].data[0][key] !== 'undefined') {
                    headers.push(key);
                }
            }
        }

        const metadata = [];
        const formData = new FormData();
        formData.append('access_token', access_token);
        formData.append('form_id', form_id);
        formData.append('scope', 'put_data');
        formData.append('header', JSON.stringify(headers));
        formData.append('batch', JSON.stringify(post));
        formData.append('metadata', JSON.stringify(metadata));
        //let blob = []
        for(let i = 0; i < images.length; i++) {
            console.log(images[i]);
            formData.append(`file${i}`, images[i]);
            //blob[i] = await new Response(formData.get(`file${i}`)).blob();
        }
        // localforage store key        
        /*
        var key = 'formdata_' + form_id;
        let save_object = {};
        formData.forEach((value, key) => save_object[key] = value);*/

        const xhr = new XMLHttpRequest();
        xhr.open("POST", "https://"+ URL +"/v"+ API_VERSION +"/pds.php");
        // Ha a szerver fogadna application/json formátumot egyszerű dolgunk lenne:
        // xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");  
        // const body = JSON.stringify(data);
        
        // de ahhoz, hogy az adatokat application/x-www-form-urlencoded formátumban küldjük el a szervernek, meg kell változtatni megfelelő formátumra. 
        // Ebben a formátumban az adatokat kulcs-érték párok formájában kell elküldeni, és ezeket az értékeket az URL-kódolás szabályai szerint kell átalakítani.
        
        xhr.onload = () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                const j = JSON.parse(xhr.responseText);
                if (j.status == 'success') {
                    resolve(true);
                }
                else {
                    resolve(false);
                    // Store form data
                    /*localforage.setItem(key, save_object).then(function() {
                        console.log("formData saved");
                    }).catch(function(err) {
                        // This code runs if there were any errors
                        console.log(err);
                    });
                    for(let i = 0; i < blob.length; i++) {
                        localforage.setItem(`${key}_images_${i}`, blob[i]).then(function (value) {
                            // Sikeres tárolás
                        }).catch(function(err) {
                            // Hiba kezelése
                        });
                    }*/
                }
            } else {
                console.log(`Error: ${xhr.status}`);
                resolve(false);
                // Store form data
                /*localforage.setItem(key, save_object).then(function() {
                    console.log("formData saved");
                }).catch(function(err) {
                    // This code runs if there were any errors
                    console.log(err);
                });
                for(let i = 0; i < blob.length; i++) {
                    localforage.setItem(`${key}_images_${i}`, blob[i]).then(function (value) {
                        // Sikeres tárolás
                    }).catch(function(err) {
                        // Hiba kezelése
                    });
                }*/
            }
        };
        xhr.send(formData);
    });
}
//const crypto = require('crypto');
function generateUniqueHash(string) {

    //const hash = crypto.createHash('sha256').update(string).digest('hex');
    const hash = CryptoJS.SHA256(string).toString();

    //const uniqueId = hash.slice(0, 32);

    return hash;
}

// lehetne ezt a függvényt is használni a JSON object URI stringgé alakítására...
function jsonToUrlEncoded(json) {
    let encodedString = '';
    for (let key in json) {
        if (json.hasOwnProperty(key)) {
            if (encodedString !== '') {
                encodedString += '&';
            }
            encodedString += encodeURIComponent(key) + '=' + encodeURIComponent(json[key]);
        }
    }
    return encodedString;
}

function Close(e) {
    let el = document.getElementById(e);
    el.style.display = "none";
}

// Tab navigator
const scrollContent = document.querySelector('.scroll-content');
const scrollItems = document.querySelectorAll('.scroll-item');
const initialClickEvent = new Event('click');
let currentIndex = 0;
function updateScroll(scrollItems,currentIndex) {
    const activeItem = scrollItems[currentIndex];

    scrollItems.forEach(item => {
      item.style.display = 'none';
    });

    activeItem.style.display = 'block';

    scrollItems.forEach(item => item.classList.remove('active'));
    activeItem.classList.add('active');
}
document.addEventListener('DOMContentLoaded', function () {
    const leftArrow = document.querySelector('.arrow.left');
    const rightArrow = document.querySelector('.arrow.right');

    function selectPrevItem() {
        currentIndex = Math.max(0, currentIndex - 1);
        updateScroll(scrollItems,currentIndex);
        triggerItemClick();
    }

    function selectNextItem() {
        currentIndex = Math.min(scrollItems.length - 1, currentIndex + 1);
        updateScroll(scrollItems,currentIndex);
        triggerItemClick();
    }

    function triggerItemClick() {
        const activeItem = scrollItems[currentIndex];
        const clickEvent = new Event('click');
        activeItem.dispatchEvent(clickEvent);
    }

    leftArrow.addEventListener('click', selectPrevItem);
    rightArrow.addEventListener('click', selectNextItem);
    
    // Inicializáció során a kívánt kattintás eseményt kiváltjuk az első elemen
    scrollItems[0].dispatchEvent(initialClickEvent); 
    
    updateScroll(scrollItems,currentIndex);
});

// Photo from input
document.getElementById('photoInput').addEventListener('change', handlePhotoSelect);

// Display photo 
function handlePhotoSelect(event) {
    const fileInput = event.target;
    
    if (fileInput.files.length > 0) {
        for (let i = 0; i < fileInput.files.length; i++) {
            displayPhoto(fileInput.files[i]);
        }
    }
}

function displayPhoto(photo) {
    const reader = new FileReader();
    
    reader.onload = function (e) {
        const div = document.createElement('div');
        div.className = 'photoContainer';
        const img = document.createElement('img');
        img.src = e.target.result;
        
        const button = document.createElement('button');
        button.textContent = 'Törlés';
        button.className = 'deleteButton';
        button.addEventListener('click', function () {
            div.remove();
        });
        
        div.appendChild(img);
        div.appendChild(button);
        
        document.getElementById('photoContainer').appendChild(div);
    };
    
    reader.readAsDataURL(photo);    

}

async function getPosition() {
    if (fixed_position.length != 0) {
        return {lon:fixed_position[0],lat:fixed_position[1]}
    }
    else if (navigator.geolocation) {
        try {
            const position = await new Promise((resolve, reject) => {
                navigator.geolocation.getCurrentPosition(resolve, reject);
            });

            let lon = position.coords.longitude;
            let lat = position.coords.latitude;
            return {lon:lon,lat:lat};
        } catch (error) {
            console.error(error);
            //if (error.message == 'User denied geolocation prompt') {
            //    alert("Engedélyezze a helyadatok használatát, és töltse be újra az alkalmazást!");
            //}
        }
    } else {
        console.error('A geolocation nem támogatott a böngésződben.');
        //alert("Úgy tűnik, hogy a helyadatok használata nem támogatott a böngésződben!");
    }
}

// GeoWatcher singleton
var GeoWatcher = (function () {
    var instance;
 
    function createInstance() {
        var object = new Object({
            watchers: [],
            lat: null,
            lon: null,
            watchId: null,
            addWatcher: function(watcher) {
                this.watchers.push(watcher);
            },
            removeWatcher: function(watcher) {
                var index = this.watchers.indexOf(watcher);
                if (index > -1) {
                    this.watchers.splice(index, 1);
                }
            },
            init: function() {
            console.log("geoWatcher initialzed");
                if (navigator.geolocation) {
                    this.watchId = navigator.geolocation.watchPosition(
                        (position) => {
                            Alpine.store('geoLocationOn',true);
                            this.lat = position.coords.latitude;
                            this.lon = position.coords.longitude;
                            this.updateWatchers();
                        },
                        (error) => {
                            if (error.code == error.PERMISSION_DENIED) {
                                alert("GPS nincs engedélyezve vagy bekapcsolva. Kérjük, engedélyezze a GPS-t a helymeghatározáshoz.");
                            }
                        }
                    );
                } else {
                    alert("A böngészője nem támogatja a geolokációt.");
                }
            },
            updateWatchers: function() {
                //this.watchers.forEach(function(watcher) {
                //    watcher.updateInput(this.lon, this.lat);
                //}, this);
                
                localStorage.setItem('lon', this.lon);
                localStorage.setItem('lat', this.lat);
                this.watchers.forEach(function(watcher) {
                    watcher.updateInput();
                }, this);
            },
            destroy: function() {
                if (this.watchId && navigator.geolocation) {
                    navigator.geolocation.clearWatch(this.watchId);
                }
            }
        });
        return object;
    }
 
    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();

function geoComponent(inputId) {
    return {
        inputId: inputId,
        init() {
            var geoWatcher = GeoWatcher.getInstance();
            geoWatcher.addWatcher(this);
            if (!geoWatcher.watchId) {
                geoWatcher.init();
            }
            this.updateInput();
        },
        updateInput() {
            var lon = localStorage.getItem('lon');
            var lat = localStorage.getItem('lat');
            var inputField = document.getElementById(this.inputId);
            if (inputField && lon && lat) {
                inputField.value = lon + ' ' + lat;
            }
        },
        /*updateInput(lon, lat) {
            var inputField = document.getElementById(this.inputId);
            if (inputField) {
                inputField.value = lon + ' ' + lat;
            }
        },*/
        destroy() {
            GeoWatcher.getInstance().removeWatcher(this);
        }
    }
}



// Location watch
/*function geoComponent(inputId) {
    return {
        lat: null,
        lon: null,
        watchId: null,
        inputId: inputId,
        init() {
            if (navigator.geolocation) {
                this.watchId = navigator.geolocation.watchPosition((position) => {
                    this.lat = position.coords.latitude;
                    this.lon = position.coords.longitude;
                    this.updateInput();
                });
            }
        },
        updateInput() {
            var inputField = document.getElementById(this.inputId);
            if (inputField) {
                inputField.value = this.lon + ' ' + this.lat;
            }
        },
        destroy() {
            if (this.watchId && navigator.geolocation) {
                navigator.geolocation.clearWatch(this.watchId);
            }
        }
    }
}*/

function locationTrack(e) {

    if (e == 'off') {
        document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route" style="color:lightgray"></i>';
        document.getElementById("trackloc").title = "Turn on location tracking";
        updateMap = false;
        return false;
    } else {
        document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route"></i>';
        document.getElementById("trackloc").title = "Turn off location tracking";
        updateMap = true;
        return true;
    }
}

// Return OpenBioMaps profile->name
async function getObserver() {
    
    access_token = getCookie("access_token");
    if (access_token == '') {
        access_token = await refreshToken();
    }

    if (access_token != '') {

        if (isJSON(access_token)) {
            let j = JSON.parse(access_token);
            access_token = j.data.access_token    // OpenBioMaps access_token is a JSON
        }

        const formData = new FormData();
        formData.append('access_token', access_token);
        formData.append('scope', 'get_profile');

        try {
            const response = await fetch("https://"+ URL +"/v"+ API_VERSION +"/pds.php", {
                method: 'POST',
                body: formData
            });

            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }

            Alpine.store('access_token_state',true);
            refresh_token = getCookie("refresh_token");
            if (refresh_token != "") {
                localforage.setItem('refresh_token', refresh_token);
            }

            const text = await response.text();
            try {
                const data = JSON.parse(text);
                return data.data;
            } catch (error) {
                console.log(`JSON Error: ${error}`);
                console.log(`Raw response: ${text}`);
            }
        } catch (error) {
            access_token = await refreshToken();
            console.log(`Fetch Error: ${error}`);
        }
    } else {
        Alpine.store('access_token_state',false);
        // refresh token error
        // got to log in process
    }
    /* Localforge test
     * localforage.setItem('key', 'value').then(function () {
        return localforage.getItem('key');
    }).then(function (value) {
        console.log(value);
    }).catch(function (err) {
        console.log(Error);
    });*/
}

// OpenStreetMap - get street name
async function getStreetName(longitude,latitude) {
    if (longitude === null || latitude === null) {
        return;
    }
    const apiUrl = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}`;
    try {
        let response = await fetch(apiUrl);

        if (!response.ok) {
            throw new Error(`Failed to fetch data from OpenStreetMap. Status: ${response.status}`);
        }

        let data = await response.json();
        
        return data;

    } catch (error) {
        console.error('Error fetching data:', error.message);
        // It's a good practice to throw the error again or return a specific value to indicate an error
        throw error;
    }
}

function gen_site_name(string) {
    var processedString = string.replace(/[^a-zA-Z0-9]/g, '');
    var randomString = random_string(5);
    return processedString + '_' + randomString;
}

function random_string(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

// OpenWeatherMap - get weather data
async function retrieveWeather(lon,lat) {
    if (lon === null || lat === null) {
        return;
    }
    const apiUrl = `https://`+ URL +`/`+ APP_PATH +`/?get_weather&lon=${lon}&lat=${lat}`;
    let posts = await (await fetch(apiUrl)).json();
    return posts;
}

// Navigation tab
function setTab(tab) {
   currentIndex = tab;
   updateScroll(scrollItems,currentIndex);
   scrollItems[tab].dispatchEvent(initialClickEvent); 
}

function listCookies() {
    var theCookies = document.cookie.split(';');
    var aString = '';
    for (var i = 1 ; i <= theCookies.length; i++) {
        aString += i + ' ' + theCookies[i-1] + "\n";
    }
    return aString;
}

function el(id) {
  return document.getElementById(id);
}

function form_elements_check(formElements) {
    
    let errors = [];
    formElements.querySelectorAll('input, select').forEach(function(element) {
        if (element.required) {
            if (element.value === '') {
                errors.push(element.id);
            }
        }
    });
    return errors;
}

// Start GeoWatcher
window.onload = function() {
/*    var userAgent = navigator.userAgent;
        if (userAgent.includes('Firefox') && userAgent.includes('Mobile')) {
            alert("A geolokáció engedélyezése problémás lehet a mobil Firefoxban. Kérjük, próbáljon meg másik böngészőt használni.");
    }
*/
    var geoWatcher = GeoWatcher.getInstance();
    //document.getElementById('startGeoWatch').onclick = function() {
        geoWatcher.init();
    //}
};
// Stop GeoWatcher
window.onunload = function() {
    var geoWatcher = GeoWatcher.getInstance();
    geoWatcher.destroy();
};

function logout() {
    localforage.setItem('refresh_token', '').then(function () {
        window.location.href = '?logout';
    }).catch(function(err) {
        console.log("Setting refresh_token failed");
    });
}
